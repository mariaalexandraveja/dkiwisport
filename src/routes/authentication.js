const express = require('express');
const router = express.Router();
const passport = require('passport');
const pool = require('../database');
const query = require('../lib/queries');
const { isLoggedIn, isLoggedOut } = require('../lib/helpers');
const { encriptar } = require('../lib/helpers');

router.get('/signup', (req, res) => {
    res.render('auth/signup');
});

router.post('/signup', async (req, res) => {
    const rows = await query.consultarUsuario(req.body.correo);
    if(rows.length <= 0) {
        const { nombre, telefono } = req.body;
        let tipo = 0; // Usuario tipo Recepcionista
        let password = await encriptar(req.body.contrasenia);
        let correo = req.body.correo;
        const newUser = {
            correo, 
            password,
            tipo,
            nombre,
            telefono
        };
        const result = await pool.query(`INSERT INTO usuarios VALUES ('${correo}', '${password}', 0, '${nombre}', ${telefono})`);
        req.flash('success', 'El recepcionista se ha registrado con exito.');
    }
    else req.flash('failure', 'El correo de ese recepcionista ya fue registrado.');
     res.redirect('/administrar');
});

router.get('/signin', isLoggedOut, (req, res) => {
    res.render('auth/signin');
});

router.post('/signin', isLoggedOut, (req, res, next) => {
    passport.authenticate('local.signin', {
        successRedirect: '/welcome',
        failureRedirect:'/signin',
        failureFlash: true
    })(req, res, next);
});

router.get('/logout', (req, res) => {
    req.logOut(req.user, err => {
        if(err) return next(err);
        res.redirect("/signin");  
    });
});

module.exports = router;