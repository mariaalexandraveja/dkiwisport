const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const pool = require('../database');
const query = require('./queries');
const { encriptar, compararContrasenia } = require('../lib/helpers');

passport.use('local.signin', new LocalStrategy({
    usernameField: 'correo',
    passwordField: 'contrasenia',
    passReqToCallback: true
}, async(req, username, password, done) => {
    const rows = await query.consultarUsuario(username);
    if(rows.length > 0) {
        const user = rows[0];
        const validPassword = await compararContrasenia(password, user.contrasenia);
        if(validPassword) {
            console.log('Entro aquí')
            done(null, user, req.flash('success', 'Welcome' + user.nombre));
        }
        else {
            done(null, false, req.flash('failure', 'Contraseña incorrecta.'));
        }
    }
    else {
        return done(null, false, req.flash('failure', 'El correo de ese usuario no existe.'));
    }
}));

 /*
passport.use('local.signup', new LocalStrategy({
    usernameField: 'correo',
    passwordField: 'contrasenia',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const rows = await query.consultarUsuario(username);
    if(rows.length <= 0) {
        const { nombre, telefono } = req.body;
        let tipo = 0; // Usuario tipo Recepcionista
        password = await encriptar(password);
        let correo = username;
        const newUser = {
            correo, 
            password,
            tipo,
            nombre,
            telefono
        };
        const result = await pool.query(`INSERT INTO usuarios VALUES ('${correo}', '${password}', 0, '${nombre}', ${telefono})`);
        return done(null, newUser);
    }
    return done(null, false, req.flash('failure', 'El correo de ese usuario ya fue registrado.')); 

    
     
}));*/

passport.serializeUser((user, done) => {
    done(null, user.correo);
});

// id = primary key of my user
passport.deserializeUser(async (id, done) => {
    const rows =  await query.consultarUsuario(id);
    done(null, rows[0]);
});