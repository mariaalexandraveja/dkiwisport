const pool = require('../database');
const querys = {};

querys.consultarUsuarios = async () => await pool.query(`SELECT * FROM usuarios WHERE tipo = 0`);

querys.consultarUsuario = async (correo) => await pool.query(`SELECT * FROM usuarios WHERE correo = '${correo}'`);

querys.addCliente = async (cliente) => await pool.query(`INSERT INTO clientes VALUES (${cliente['id_cliente']}, '${cliente['nombre_cliente']}', '${cliente['nombre_equipo']}', '${cliente['telefono']}', '${cliente['usuario']}') `);

querys.consultarClientePorID = async (cliente) => await pool.query(`SELECT * FROM clientes WHERE id_Cliente = ${cliente['id_cliente']}`);

querys.consultarCliente = async (cliente) => await pool.query(`SELECT * FROM clientes WHERE id_Cliente = ${cliente['id_cliente']} AND nombre_EQUIPO = '${cliente['nombre_equipo']}'`);

/** PEDIDOS CRUD */

//Create

querys.addPedido = async (pedido) => await pool.query(`INSERT INTO pedidos (id_Cliente, id_Usuario, fecha_inicio, direccion, pais, ciudad, descripcion, costo, estado_pedido, nombre_EQUIPO) VALUES (${pedido['id_cliente']}, '${pedido['usuario']}', SYSDATE(), '${pedido['direccion']}', '${pedido['pais']}', '${pedido['ciudad']}', '${pedido['descripcion']}', ${pedido['costo']}, 'Iniciado',  '${pedido['nombre_equipo']}')`);

//Read

    // Por usuario

querys.consultarPedidosPorID_U = async (id, correo) => await pool.query(`SELECT * FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente) WHERE p.id_Pedido = ${id} AND p.id_Usuario = '${correo}' AND estado_pedido = 'Iniciado' ORDER BY id_Pedido`);

querys.consultarPedidosPorCliente_U = async (id, correo) => await pool.query(`SELECT * FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente) WHERE p.id_Cliente = ${id} AND p.id_Usuario = '${correo}' AND estado_pedido = 'Iniciado' ORDER BY id_Pedido`);

querys.consultarPedidosPorNombreEquipo_U = async (nombre_equipo, correo) => await pool.query(`SELECT * FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente) WHERE p.nombre_EQUIPO = '${nombre_equipo}' AND p.id_Usuario = '${correo}' AND estado_pedido = 'Iniciado' ORDER BY id_Pedido`);

querys.consultarPedidosPorFechaInicio_U = async (fecha, correo) => await pool.query(`SELECT * FROM pedidos WHERE fecha_inicio >= '${fecha}' AND   id_Usuario = '${correo}'`);

querys.consultarPedidosIniciados_U = async (correo) => await pool.query(`SELECT * FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente) WHERE p.id_Usuario = '${correo}' AND estado_pedido = 'Iniciado' ORDER BY id_Pedido`);

    // General

let informacion = "p.id_Pedido, p.id_Usuario, p.fecha_inicio, p.fecha_fin, p.direccion, p.pais, p.ciudad, p.descripcion, p.costo, p.estado_pedido, p.nombre_EQUIPO, p.id_Cliente, c.nombre, c.telefono, u.nombre recepcionista, u.telefono telefono_recepcionista";

querys.consultarPedidos = async () => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo)`);

querys.consultarPedidosPorID = async (id) => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE p.id_Pedido = ${id} ORDER BY p.id_Pedido`);

querys.consultarPedidosPorCliente = async (id) => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE p.id_Cliente = ${id} ORDER BY p.id_Pedido`);

querys.consultarPedidosPorNombreEquipo = async (nombre_equipo) => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE p.nombre_EQUIPO = '${nombre_equipo}' ORDER BY p.id_Pedido`);

querys.consultarPedidosPorFechaInicio = async (fecha) => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE fecha_inicio <= '${fecha}'`);

querys.consultarPedidosPorUsuario = async (correo) => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE p.id_Usuario = '${correo}' ORDER BY p.id_Pedido`);

querys.consultarPedidosIniciados = async () => await pool.query(`SELECT ${informacion} FROM pedidos p JOIN clientes c ON(p.id_Cliente = c.id_Cliente AND p.nombre_EQUIPO = c.nombre_EQUIPO) JOIN usuarios u ON(c.usuario = u.correo) WHERE estado_pedido = 'Iniciado' ORDER BY p.id_Pedido`);

//Delete

querys.eliminarPedido = async (id) => await pool.query(`DELETE FROM pedidos WHERE id_Pedido=${id}`);

//Update

querys.actualizarPedido = async (id) => await pool.query(`UPDATE pedidos SET estado_pedido = 'Finalizado', fecha_fin = SYSDATE() WHERE id_Pedido=${id}`);

// CRUD PRODUCTOS

querys.consultarProductos = async (id) => await pool.query(`SELECT * FROM productos WHERE id_Pedido = ${id}`);

querys.addProductos = async (productos, id_pedido) => {
    let add = [];
    let j = 1;
    for(i in productos) {
        await pool.query(`INSERT INTO productos (id_Pedido, tipo, sexo, color, sublimado, bordado, estampado, cantidad, costo, descripcion) VALUES (${id_pedido}, '${productos[i][`tipo_producto#${j}`]}', '${productos[i][`sexo#${j}`]}', '${productos[i][`color#${j}`]}', ${productos[i][`sublimado#${j}`]}, ${productos[i][`bordado#${j}`]}, ${productos[i][`estampado#${j}`]}, ${productos[i][`cantidad#${j}`]}, ${productos[i][`costo#${j}`]}, '${productos[i][`descripcion#${j}`]}')`);
        j++;
    }
};

module.exports = querys;